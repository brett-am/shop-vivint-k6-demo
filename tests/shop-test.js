import {check, sleep} from 'k6';
import http from 'k6/http';
import { Rate } from 'k6/metrics';

export let errorRate = new Rate('errors');
export let options = {
    stages: [
        {duration: '10s', target: 5},
        {duration: '5s', target: 5},
        {duration: '5s', target: 0},
    ],
    thresholds: {
        errors: ['rate<0.1'], // <10% errors
        http_req_duration: [
            'p(90) < 2200',
            'p(95) < 2300',
            'p(99.9) < 3000'
        ]
    },
};

export default function () {
    let params = {
        headers: { 'sessionId': '9ff5087b-d854-46ee-bb48-c342f9167779' },
    };
    let res = http.get('https://apio.vivint.com/Commerce/v1/shopbe', params);
    check(res, {
        'status was 200': (r) => r.status == 200,
        'body size is 5701 +/- 1 bytes': (r) => (r.body.length >= 5700 && r.body.length <= 5702),
        'Shop key present': (r) => r.json("catalog.name") == "Shop"
    });
    errorRate.add(!res);
    sleep(1);
}